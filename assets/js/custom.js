/*=============================================================
    Authour URI: www.binarytheme.com
    License: Commons Attribution 3.0

    http://creativecommons.org/licenses/by/3.0/

    100% To use For Personal And Commercial Use.
    IN EXCHANGE JUST GIVE US CREDITS AND TELL YOUR FRIENDS ABOUT US
   
    ========================================================  */


(function ($) {
    "use strict";
    var mainApp = {

        main_fun: function () {
            /*====================================
             CUSTOM LINKS SCROLLING FUNCTION 
            ======================================*/

            $('header a[href*=#]').click(function () {
                if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                    && location.hostname == this.hostname) {
                    var $target = $(this.hash);
                    $target = $target.length && $target
                        || $('[name=' + this.hash.slice(1) + ']');
                    if ($target.length) {
                        var targetOffset = $target.offset().top;
                        $('html,body')
                            .animate({scrollTop: targetOffset}, 800); //set scroll speed here
                        return false;
                    }
                }
            });


            /*====================================
               WRITE YOUR SCRIPTS BELOW 
           ======================================*/


        },

        initialization: function () {
            mainApp.main_fun();

        }

    }
    // Initializing ///

    $(document).ready(function () {
        mainApp.main_fun();
    });

}(jQuery));


var h_hght = 0; // высота шапки
var h_mrg = 0;    // отступ когда шапка уже не видна

$(function () {
    if ($(window).width() > 989) {
        h_hght = 0;
    } else {
        h_hght = 0;
    }

    var elem = $('#top_nav');
    var top = $(this).scrollTop();

    if (top > h_hght) {
        elem.css('top', h_mrg);
    }

    $(window).scroll(function () {
        top = $(this).scrollTop();
        if (h_hght != 0) {
            if (top + h_mrg < h_hght) {
                elem.css('top', (h_hght - top));
            } else {
                elem.css('top', h_mrg);
            }
        }
    });

});
// $(window).scroll(function(){
//     var docscroll=$(document).scrollTop();
//     if(docscroll>$(window).height()){
//         $('nav').css({'height': $('nav').height(),'width': $('nav').width()}).addClass('fixed');
//     }else{
//         $('nav').removeClass('fixed');
//     }
// });

$(document).ready(function () {
    var isMobile = {
        Android: function () {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function () {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function () {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function () {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function () {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function () {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };
    if (!isMobile.any()) {
        $('#popup__toggle').attr("data-toggle", "modal");
        $('#popup__toggle').attr("data-target", "#myModal");
    }
    else {
    // $('#popup__toggle').attr("data-toggle", "modal");
    // $('#popup__toggle').attr("data-target", "#myModal");
    $('#popup__toggle').attr("href", "tel:+74957443925");
    }
    $("#inputPhone").mask("+7 ?(999) 999 99 99");

});