function inArray(needle, haystack) {
    var length = haystack.length;
    for (var i = 0; i < length; i++) {
        if (typeof haystack[i] == 'object') {
            if (arrayCompare(haystack[i], needle))return true;
        } else {
            if (haystack[i] == needle)return true;
        }
    }
    return false;
}
window.isset = function (v) {
    if (typeof(v) == 'object' && v == 'undefined') {
        return false;
    } else if (arguments.length === 0) {
        return false;
    } else {
        var buff = arguments[0];
        for (var i = 0; i < arguments.length; i++) {
            if (typeof(buff) === 'undefined' || buff === null)return false;
            buff = buff[arguments[i + 1]];
        }
    }
    return true;
}
function myconf() {
    var cf = $.Deferred();
    $.ajax({
        type: 'POST', url: '/feedback.php', dataType: 'json', data: 'act=cfg', success: function (answer) {
            cf.resolve(answer.configs);
        }
    });
    return cf;
}
var mcf = myconf();
mcf.done(function (conf) {
    $(document).ready(function () {
        (function () {
            var fb = $('.feedback');
            if (fb.length > 0) {
                fb.each(function () {
                    var form = $(this).closest('form'), name = form.attr('name');
                    if (isset(conf[name]) && isset(conf[name].cfg.antispamjs)) {
                        $(form).prepend('<input type="text" name="' + conf[name].cfg.antispamjs + '" value="tesby" style="display:none;">');
                    }
                });
            }
        })();
    });
    function feedback(vars) {
        var bt = $(vars.form).find('.feedback');
        var btc = bt.clone();
        var bvc = bt.val();
        var cfg = conf[vars.act].cfg;
        alert = $('#alert');
        alert.html('');
        var is_error = false;
        if ($('#inputPhone').val().length != 18 ){
            alert.append('Введите номер до конца!')
            is_error = true;
        }
        if ($('#inputName').val().length == 0 ){
            if (is_error){
                alert.append('<br>')
            }
            alert.append('Поле имя не может быть пустым')
            is_error = true;
        }
        if (!is_error){
            $.ajax({
                type: 'POST',
                url: '/feedback.php',
                cache: false,
                dataType: 'json',
                data: 'act=' + vars.act + '&' + vars.data,
                beforeSend: function () {
                    $(bt).prop("disabled", true);
                },
                success: function (answer) {
                    var obj = answer.errors;
                    if (answer.errors != undefined) {
                        var error = '';
                        for (var param in obj) {
                            if (obj.hasOwnProperty(param)) {
                                error = error + '<li>' + obj[param] + '</li>';
                            }
                        }
                        alert.html('');
                        alert.html(error);
                    } else {
                        alert.html('');
                        alert.html('Заявка успешно отправлена');
                    }
                }
            });
        }
    }

    $(document).on('mouseenter mouseover', '.feedback', function () {
        var form = $(this).closest('form'), name = form.attr('name');
        if (isset(conf[name]) && isset(conf[name].cfg.antispamjs)) {
            $('input[name=' + conf[name].cfg.antispamjs + ']').val('');
        }
    });
    $(document).on('click', '.feedback', function () {
        var form = $(this).closest('form'), name = form.attr('name'), obj = {};
        obj.form = form;
        obj.act = name;
        obj.data = $(form).serialize();
        feedback(obj);
        return false;
    });

});